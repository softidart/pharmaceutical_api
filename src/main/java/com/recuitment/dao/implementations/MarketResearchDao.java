package com.recuitment.dao.implementations;

import com.recuitment.dao.IMarketResearchDao;
import com.recuitment.model.MarketResearch;

/**
 * Created by Administrador on 08/08/2017.
 */
public class MarketResearchDao extends GenericCrudDao<MarketResearch> implements IMarketResearchDao{
}
