package com.recuitment.dao.implementations;

import com.recuitment.dao.IBeautyProductDao;
import com.recuitment.model.BeautyProduct;

/**
 * Created by Administrador on 08/08/2017.
 */
public class BeautyProductDao extends GenericCrudDao<BeautyProduct> implements IBeautyProductDao {
}
