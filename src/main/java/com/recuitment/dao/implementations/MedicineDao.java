package com.recuitment.dao.implementations;

import com.recuitment.dao.IMedicineDao;
import com.recuitment.model.Medicine;
import com.recuitment.model.StockElement;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrador on 08/08/2017.
 */
public class MedicineDao extends GenericCrudDao<Medicine> implements IMedicineDao {

    @Transactional
    public List<Medicine> getAllElements() {
        try{
            Criteria criteria = entityManager.unwrap(Session.class)
                    .createCriteria(Medicine.class);
            return (List<Medicine>)criteria.list();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
