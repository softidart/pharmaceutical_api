package com.recuitment.dao.implementations;


import com.recuitment.dao.IStockElementDao;
import com.recuitment.model.StockElement;

/**
 * Created by Administrador on 08/08/2017.
 */

public class StockElementDao extends GenericCrudDao<StockElement> implements IStockElementDao {

}
