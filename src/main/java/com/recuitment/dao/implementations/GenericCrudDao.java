package com.recuitment.dao.implementations;

import com.recuitment.dao.IGenericCrudDao;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by Administrador on 08/08/2017.
 */
public abstract class GenericCrudDao <T> implements IGenericCrudDao<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    private Class<T> type;

    public GenericCrudDao() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public T create(final T t) {
        entityManager.persist(t);
        return t;
    }

    @Override
    public void delete(final Object id) {
        entityManager.remove(entityManager.getReference(type, id));
    }

    @Override
    public T find(final Object id) {
        return (T) entityManager.find(type, id);
    }

    @Override
    public T update(final T t) {
        return entityManager.merge(t);
    }
}
