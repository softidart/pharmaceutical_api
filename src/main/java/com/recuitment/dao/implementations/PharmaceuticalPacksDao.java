package com.recuitment.dao.implementations;

import com.recuitment.dao.IPharmaceuticalPacksDao;
import com.recuitment.dto.PackDto;
import com.recuitment.model.StockElement;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrador on 09/08/2017.
 */
public class PharmaceuticalPacksDao implements IPharmaceuticalPacksDao {

    @PersistenceContext
    protected EntityManager entityManager;

    /**
     * Get a list of packDto
     */
    @Transactional
    public List<PackDto> getPacksDtoList() {
        try{
            // List of Dto to get
            List<PackDto> dtoList = new ArrayList<PackDto>();

            String queryString = "SELECT DISTINCT BP.name, M.name, ((SELECT SEI.price FROM StockElement SEI WHERE SEI.id = BP.id) + (SELECT SEI.price FROM StockElement SEI WHERE SEI.id = M.id)) * 0.75 AS packCost, M.expiration_date, BP.expiration_date, (((SELECT SEI.price FROM StockElement SEI WHERE SEI.id = BP.id) + (SELECT SEI.price FROM StockElement SEI WHERE SEI.id = M.id)) - ((SELECT SEI.cost FROM StockElement SEI WHERE SEI.id = BP.id) + (SELECT SEI.cost FROM StockElement SEI WHERE SEI.id = M.id))) as profit FROM BeautyProduct BP, Medicine AS M ORDER BY profit DESC";
            List<Object[]> resultList = entityManager.createQuery(queryString)
                    .getResultList();

            // Map the values returned by the query
            for (Object[] result : resultList){
                PackDto dto = new PackDto(
                        (String)result[0],
                        (String)result[1],
                        (Double)result[2]
                );

                dto.setProfit((Double)result[5]);

                Date medicineExpiration = (Date)result[3];
                Date beautyProductExpiration = (Date)result[4];

                dto.setExprirationDate(
                        medicineExpiration.before(beautyProductExpiration) ?
                                medicineExpiration : beautyProductExpiration );

                dtoList.add(dto);
            }

            return dtoList;

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }

}
