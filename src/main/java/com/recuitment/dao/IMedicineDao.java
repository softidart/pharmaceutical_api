package com.recuitment.dao;

import com.recuitment.model.Medicine;

import java.util.List;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IMedicineDao extends IGenericCrudDao<Medicine>{
    List<Medicine> getAllElements();
}
