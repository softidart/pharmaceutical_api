package com.recuitment.dao;

import com.recuitment.model.MarketResearch;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IMarketResearchDao extends IGenericCrudDao<MarketResearch>{
}
