package com.recuitment.dao;

import com.recuitment.model.StockElement;

import java.util.List;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IStockElementDao extends IGenericCrudDao<StockElement>{

}
