package com.recuitment.dao;

import java.util.Map;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IGenericCrudDao<T> {

    T create(T t);

    void delete(Object id);

    T find(Object id);

    T update(T t);
}
