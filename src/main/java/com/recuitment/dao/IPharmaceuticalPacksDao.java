package com.recuitment.dao;

import com.recuitment.dto.PackDto;

import java.util.List;

/**
 * Created by Administrador on 09/08/2017.
 */
public interface IPharmaceuticalPacksDao {
    public List<PackDto> getPacksDtoList();
}
