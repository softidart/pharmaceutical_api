package com.recuitment.dao;

import com.recuitment.model.Stock;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IStockDao extends IGenericCrudDao<Stock> {
}
