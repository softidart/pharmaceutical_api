package com.recuitment.dao;

import com.recuitment.model.BeautyProduct;

/**
 * Created by Administrador on 08/08/2017.
 */
public interface IBeautyProductDao extends IGenericCrudDao<BeautyProduct> {
}
