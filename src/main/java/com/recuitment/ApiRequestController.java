package com.recuitment;

import com.recuitment.dto.PackDto;
import com.recuitment.model.Medicine;
import com.recuitment.service.IPharmaceuticalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by Administrador on 08/08/2017.
 */
@RestController
@EnableAutoConfiguration
public class ApiRequestController {

    @Autowired
    private IPharmaceuticalService apiRequestService;

    private static final Logger logger = LoggerFactory.getLogger(ApiRequestController.class);

    @RequestMapping(value = "/packs", method = RequestMethod.GET)
    public ResponseEntity<List<PackDto>>  showPacks() {

        List<PackDto> packList = apiRequestService.getPack();

        if (packList == null || packList.isEmpty()){
            logger.info("no users found");
            return new ResponseEntity<List<PackDto>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<PackDto>>(packList, HttpStatus.OK);
    }

    @RequestMapping(value = "/medicine", method = RequestMethod.GET)
    public ResponseEntity<List<Medicine>> showMedicineList() {
        List medicineList = apiRequestService.getMedicineList();

        if (medicineList == null || medicineList.isEmpty()){
            logger.info("no medicine list found");
            return new ResponseEntity<List<Medicine>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Medicine>>(medicineList, HttpStatus.OK);
    }

    @RequestMapping(value = "/medicine/{id}", method = RequestMethod.GET)
    public ResponseEntity<Medicine> showMedicineById(@PathVariable long id) {
        if(id <= 0) {
            return new ResponseEntity<Medicine>(HttpStatus.BAD_REQUEST);
        }

        Medicine medicine = apiRequestService.getMedicineById(id);

        if (medicine == null){
            logger.info(  GlobalVars.STOCK_ELEMENT_NOT_EXIST_MESSAGE);
            return new ResponseEntity<Medicine>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Medicine>(medicine, HttpStatus.OK);
    }

    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getHome() {
        logger.info(GlobalVars.API_LOAD_SUCCESSFULLY);

        Map<String, String> response = new HashMap<String, String>();
        try {
            response.put("status", "success");
        } catch (Exception e) {
            logger.error(GlobalVars.API_LOAD_ERROR_MESSAGE, e);
            response.put("status", "fail");
        }

        return response;
    }

    @RequestMapping(value = "/make_medicine/{name}/{price}/{cost}/{weight}/{vendor}/{date}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> makeMedicine(@PathVariable("name") String name,
                                            @PathVariable("price") Double price,
                                            @PathVariable("cost") Double cost,
                                            @PathVariable("weight") Double weight,
                                            @PathVariable("vendor") String vendor,
                                            @PathVariable("date") String date) {

        Map<String, String> response = new HashMap<String, String>();

        if(name == "" || price <= 0 || cost <= 0
                || weight <= 0 || vendor == ""){
            response.put("status", "incomplete");
        }

        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
            Date inputDate = dateFormat.parse(date);

            Medicine medicine = new Medicine();
            medicine.setName(name);
            medicine.setPrice(price);
            medicine.setCost(cost);
            medicine.setExpiration_date(inputDate);
            medicine.setWeigth(weight);
            medicine.setVendor(vendor);

            apiRequestService.createMedicine(medicine);
            response.put("status", "success");
        } catch (Exception e) {
            logger.error(GlobalVars.API_LOAD_ERROR_MESSAGE, e);
            response.put("status", "fail");
        }

        return response;
    }

    @RequestMapping(value = "/helloWorld", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getTestRequest() {
        return GlobalVars.TEST_HELLO_WORLD_MESSAGE;
    }

}
