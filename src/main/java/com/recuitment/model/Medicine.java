package com.recuitment.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 08/08/2017.
 */


@Entity
@Table(name = "MEDICINE")
public class Medicine extends StockElement {
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiration_date;
    private Double weigth;

    public Medicine() {}

    public Medicine(String name, Date expiration_date, Double weigth) {
        this.name = name;
        this.expiration_date = expiration_date;
        this.weigth = weigth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public Date getExpiration_date()
    {
        return expiration_date;
    }

    public void setWeigth(Double weigth) {
        this.weigth = weigth;
    }

    public Double getWeigth()
    {
        return weigth;
    }
}
