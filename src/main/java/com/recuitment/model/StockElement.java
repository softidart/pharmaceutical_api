package com.recuitment.model;

import javax.persistence.*;

/**
 * Created by Administrador on 08/08/2017.
 */
@Entity
@Table(name="STOCK_ELEMENT")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class StockElement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private double price;

    private double cost;

    private String vendor;

    @OneToOne(cascade = CascadeType.ALL)
    Stock stock;

    public StockElement() {
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setStock(Stock stock)
    {
        this.stock = stock;
    }

    public Stock getStock()
    {
        return stock;
    }

    @Override
    public String toString() {
        return "Stock_Element{" +
                "id=" + id +
                ", price=" + price +
                ", cost=" + cost +
                ", vendor='" + vendor + '\'' +
                '}';
    }
}
