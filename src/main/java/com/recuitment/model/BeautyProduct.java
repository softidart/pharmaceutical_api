package com.recuitment.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 08/08/2017.
 */

@Entity
@Table(name = "BEAUTY_PRODUCT")
public class BeautyProduct extends StockElement  {
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiration_date;

    @OneToOne(cascade = CascadeType.ALL)
    MarketResearch market_research;

    public BeautyProduct() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public Date getExpiration_date()
    {
        return expiration_date;
    }

    public void setMarket_research(MarketResearch market_research)
    {
        this.market_research = market_research;
    }

    public MarketResearch getMarket_research()
    {
        return market_research;
    }
}
