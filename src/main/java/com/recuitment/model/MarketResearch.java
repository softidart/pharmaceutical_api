package com.recuitment.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrador on 08/08/2017.
 */
@Entity
@Table(name = "MARKET_RESEARCH")
public class MarketResearch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Date expiration_date;

    @OneToOne
    BeautyProduct beauty_product;

    public MarketResearch() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public Date getExpiration_date()
    {
        return expiration_date;
    }

    public void setMarket_research(BeautyProduct beauty_product)
    {
        this.beauty_product = beauty_product;
    }

    public BeautyProduct getMarket_research()
    {
        return beauty_product;
    }
}
