package com.recuitment.model;

import javax.persistence.*;

/**
 * Created by Administrador on 08/08/2017.
 */
@Table(name="STOCK")
@Entity
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer quantity;

    @OneToOne
    private StockElement stockElement;

    public StockElement getStockElement() {
        return stockElement;
    }

    public void setStockElement(StockElement stockElement) {
        this.stockElement = stockElement;
    }

    public Stock() {}

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity()
    {
        return quantity;
    }
}
