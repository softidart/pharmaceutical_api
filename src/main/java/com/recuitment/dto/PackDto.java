package com.recuitment.dto;

import java.util.Date;

/**
 * Created by Administrador on 09/08/2017.
 */
public class PackDto {

    public String beautifulProductName;
    public String medicineName;
    public Double price;
    public Double profit;
    public Date exprirationDate;

    public PackDto(String beautifulProductName, String medicineName, Double price) {
        this.beautifulProductName = beautifulProductName;
        this.medicineName = medicineName;
        this.price = price;
    }

    public Double getProfit() {
        return profit;
    }

    public void setProfit(Double profit) {
        this.profit = profit;
    }

    public void setExprirationDate(Date exprirationDate) {
        this.exprirationDate = exprirationDate;
    }

    public Date getExprirationDate() {
        return exprirationDate;
    }

    public String getBeautifulProductName() {
        return beautifulProductName;
    }

    public void setBeautifulProductName(String beautifulProductName) {
        this.beautifulProductName = beautifulProductName;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
