package com.recuitment;

import com.recuitment.dao.*;
import com.recuitment.dao.implementations.*;
import org.h2.server.web.WebServlet;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * Created by Administrador on 08/08/2017.
 */
@Configuration
public class BeanConfiguration {

    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }

    @Bean
    public IPharmaceuticalPacksDao getStockElementDao() {
        return new PharmaceuticalPacksDao();
    }

    @Bean
    public IStockElementDao getDao() {
        return new StockElementDao();
    }

    @Bean
    public IBeautyProductDao getBeautifulProductDao() {
        return new BeautyProductDao();
    }

    @Bean
    public IMarketResearchDao getMarketResearchDao() {
        return new MarketResearchDao();
    }

    @Bean
    public IMedicineDao getMedicineDao() {
        return new MedicineDao();
    }

    @Bean
    public IStockDao getStockDao() {
        return new StockDao();
    }
}
