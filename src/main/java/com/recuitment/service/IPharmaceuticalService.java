package com.recuitment.service;

import com.recuitment.dto.PackDto;
import com.recuitment.model.Medicine;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * Created by Administrador on 08/08/2017.
 */

@Repository
public interface IPharmaceuticalService {
    /**
     * Get a list of medicine entities
     */
    List<Medicine> getMedicineList();
    /**
     * Get medicine entity by ID
     * @param id entity id to find.
     */
    Medicine getMedicineById(long id);
    /**
     * Create a medicine row.
     * @param medicine  entity to save,
     */
    void createMedicine(Medicine medicine);
    /**
     * Displays a pack-type DTO with values [Beauty Product Name, Medicine Name, Pack Price, Expiration Date].
     */
    List<PackDto> getPack();
}
