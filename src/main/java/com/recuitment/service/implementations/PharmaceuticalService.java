package com.recuitment.service.implementations;

import java.util.List;

import com.recuitment.GlobalVars;
import com.recuitment.dao.IMedicineDao;
import com.recuitment.dao.IPharmaceuticalPacksDao;
import com.recuitment.dto.PackDto;
import com.recuitment.model.Medicine;
import com.recuitment.service.IPharmaceuticalService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrador on 08/08/2017.
 */
@Service
@Transactional
public class PharmaceuticalService implements IPharmaceuticalService {

    final static Logger logger = Logger.getLogger(PharmaceuticalService.class);

    @Autowired
    private IMedicineDao medicineDao;

    @Autowired
    private IPharmaceuticalPacksDao pharmaceuticalPacksDao;

    /**
     * Get medicine entity by ID
     * @param id entity id to find.
     */
    @Transactional(noRollbackFor = Exception.class)
    public Medicine getMedicineById(long id)
    {
        logger.info(GlobalVars.ENTITY_WAS_GETTED_MESSAGE);
        return medicineDao.find(id);
    }

    /**
     * Get a list of medicine entities
     */
    @Transactional(noRollbackFor = Exception.class)
    public List<Medicine> getMedicineList()
    {
        logger.info(GlobalVars.ENTITY_LIST_WAS_GETTED_MESSAGE);
        return medicineDao.getAllElements();
    }

    /**
     * Create a medicine row.
     * @param medicine  entity to save,
     */
    @Transactional(noRollbackFor = Exception.class)
    public void createMedicine(Medicine medicine)
    {
        try{
            medicineDao.create(medicine);
            logger.info(GlobalVars.ENTITY_SAVED_MESSAGE);
        }catch(Exception ex){
            logger.error(GlobalVars.CRUD_ERROR_MESSAGE, ex);
        }
    }

    /**
     * Displays a pack-type DTO with values [Beauty Product Name, Medicine Name, Pack Price, Expiration Date].
     */
    @Transactional(noRollbackFor = Exception.class)
    public List<PackDto> getPack()
    {
        try{
            List<PackDto> dtoList = pharmaceuticalPacksDao.getPacksDtoList();
            logger.info(GlobalVars.PACK_LIST_GETTED_MESSAGE);
            return dtoList;
        }catch(Exception ex){
            logger.error(GlobalVars.CRUD_ERROR_MESSAGE, ex);
            return null;
        }
    }
}