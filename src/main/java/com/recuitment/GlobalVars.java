package com.recuitment;

/**
 * Created by Administrador on 08/08/2017.
 */
public final class GlobalVars {
    public static final String STOCK_ELEMENT_NOT_EXIST_MESSAGE = "Element not exist";

    public static final String ENTITY_SAVED_MESSAGE = "The entity was successfully saved";

    public static final String CRUD_ERROR_MESSAGE = "Error occurred while trying to process api request";

    public static final String API_LOAD_ERROR_MESSAGE ="Error occurred while trying to process api request";

    public static final String API_LOAD_SUCCESSFULLY ="Api request received";

    public static final String TEST_HELLO_WORLD_MESSAGE ="Hello world I'm rocking";

    public static final String PACK_LIST_GETTED_MESSAGE ="The list of pack was successfully obtained.";

    public static final String ENTITY_WAS_GETTED_MESSAGE ="The entity was obtained";

    public static final String ENTITY_LIST_WAS_GETTED_MESSAGE ="The entity list was obtained";
}
