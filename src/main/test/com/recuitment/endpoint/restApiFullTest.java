package com.recuitment.endpoint;

import com.recuitment.ApiRequestController;
import com.recuitment.GlobalVars;
import com.recuitment.TestBeanConfiguration;
import com.recuitment.dto.PackDto;
import com.recuitment.model.Medicine;
import com.recuitment.service.IPharmaceuticalService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrador on 10/08/2017.
 */

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBeanConfiguration.class})
public class restApiFullTest {
    private MockMvc mockMvc;

    @Mock
    private IPharmaceuticalService pharmaceuticalService;

    @InjectMocks
    private ApiRequestController apiRequestController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(apiRequestController)
                .build();
    }

    @Test
    public void testApiRequest() throws Exception {
        String expectedValue = GlobalVars.TEST_HELLO_WORLD_MESSAGE;
        ApiRequestController req = new ApiRequestController();
        String actualMessage = req.getTestRequest();
        assertEquals(expectedValue, actualMessage);
    }

    @Test
    public void test_get_medicine_by_id() throws Exception {

        long expectedId = 1;
        Medicine expectedMedicine = new Medicine("Ribotril", new Date(), 2.0);
        expectedMedicine.setPrice(200.0);
        expectedMedicine.setCost(150);
        expectedMedicine.setId(expectedId);

        when(pharmaceuticalService.getMedicineById(expectedId)).thenReturn(expectedMedicine);

        mockMvc.perform(get("/medicine/{id}", expectedId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price", is(expectedMedicine.getPrice())))
                .andExpect(jsonPath("$.cost", is(expectedMedicine.getCost())))
                .andExpect(jsonPath("$.name", is(expectedMedicine.getName())));

        verify(pharmaceuticalService, times(1)).getMedicineById(expectedId);
        verifyNoMoreInteractions(pharmaceuticalService);
    }

    @Test
    public void test_get_medicine_list() throws Exception {
        List<Medicine> medicineList = Arrays.asList(
                new Medicine("Ribotril", new Date(), 0.7),
                new Medicine("Brudex", new Date(), 4.1));

        when(pharmaceuticalService.getMedicineList()).thenReturn(medicineList);

        mockMvc.perform(get("/medicine"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("Ribotril")))
                .andExpect(jsonPath("$[0].weigth", is(0.7)))
                .andExpect(jsonPath("$[1].name", is("Brudex")))
                .andExpect(jsonPath("$[1].weigth", is(4.1))
                );

        verify(pharmaceuticalService, times(1)).getMedicineList();
        verifyNoMoreInteractions(pharmaceuticalService);
    }


    @Test
    public void test_get_all_packs() throws Exception {
        List<PackDto> users = Arrays.asList(
                new PackDto("ARCONA", "SUSTANON", 120.0),
                new PackDto("SHIDEIDO AQUALABEL", "LEVON", 32.6));

        when(pharmaceuticalService.getPack()).thenReturn(users);

        mockMvc.perform(get("/packs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].beautifulProductName", is("ARCONA")))
                .andExpect(jsonPath("$[0].price", is(120.0)))
                .andExpect(jsonPath("$[1].medicineName", is("LEVON")))
                .andExpect(jsonPath("$[1].price", is(32.6)));

        verify(pharmaceuticalService, times(1)).getPack();
        verifyNoMoreInteractions(pharmaceuticalService);
    }
}
