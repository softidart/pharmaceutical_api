
$(document).ready(function(){
    var date_input=$('input[name="date"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    };
    date_input.datepicker(options);
})

var app = angular.module("pharmaceuticalPackList", []);


app.controller("PharmaceuticaApi", function($scope, $http, $filter) {

    var $baseUrl = 'http://localhost:8080/'

    $scope.showList = function () {
        $http.get($baseUrl + 'packs').
        then(function(response) {
            $scope.packList = response.data;
        });
    }

    $scope.Init = function(){
        $scope.showList();
    }


    $scope.makeMedicine = function() {

        if( !$("#name-imput").val() ) {
            alert("Name is empty");
            !$("#name-imput").focus();
            return;
        }

        if( !$("#price-input").val() ) {
            alert("Price is empty");
            !$("#price-input").focus();
            return;
        }

        if( !$("#cost-input").val() ) {
            alert("Cost is empty");
            !$("#cost-input").focus();
            return;
        }

        if( !$("#expiration").val() ) {
            alert("Expiration date is empty");
            !$("#expiration").focus();
            return;
        }

        if( !$("#weight-input").val() ) {
            alert("Weight is empty");
            !$("#weight-input").focus();
            return;
        }

        if( !$("#vendor-imput").val() ) {
            alert("Vendor is empty");
            !$("#vendor-imput").focus();
            return;
        }

        // expiration date
        var date = $("#expiration").datepicker({ dateFormat: 'yyyy-mm-dd' }).val();
        var res = date.split("/");

        var day = res[0];
        var month = res[1];
        var year = res[2];

        var formatApidate = year + "-" + month + "-" + day;

        var $requestUrl = $baseUrl + 'make_medicine/' + $scope.medicine.name +
            '/' + $scope.medicine.price + '/' + $scope.medicine.cost + '/' +
            $scope.medicine.weiht + '/' + $scope.medicine.vendor + '/' +
            formatApidate;


        $http.get($requestUrl).
        then(function(response) {

            if(response.data.status == 'success'){
                alert("A new row was created");
                location.reload();
            }else{
                alert("Incorrect data, please validate.");
            }
        });
    }
});

